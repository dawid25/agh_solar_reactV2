import React from 'react';
import Navbar from './components/Navbar';
import './App.css';
import Home from './components/pages/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import GPSPosition from './components/pages/GPS';
import Camera from './components/pages/Camera';
import InfluxDB from './components/pages/InfluxDB';
import Sidebar from './components/Sidebar';

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/gpsposition' component={GPSPosition} />
          <Route path='/camera' component={Camera} />
          <Route path='/influxdb' component={InfluxDB} />
          <Route path='/sidebar' component={Sidebar} />
        </Switch>
      </Router>
    </>
  );
}

export default App;