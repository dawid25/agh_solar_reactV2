import React, { useState, useEffect } from 'react';
import { Button } from './Button';
import { Link } from 'react-router-dom';
import './Navbar.css';

function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
            AGH SOLAR Boat
            <img className='navbar-logo-solar' src='images/solarboat.png' alt="logo-stopka" />
          </Link>
          <div className='menu-icon' onClick={handleClick}>
            <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
          </div>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                to='/gpsposition'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Where am I?
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                to='/camera'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                POV
              </Link>
            </li>

            <li className='nav-item'>
              <Link
                to='/sidebar'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Sidebar
              </Link>
            </li>

            <li>
              <Link
                to='/influxdb'
                className='nav-links-mobile'
                onClick={closeMobileMenu}
              >
                Charts
              </Link>
            </li>
          </ul>
          {button && <Button buttonStyle='btn--outline'>Charts</Button>}
        </div>
      </nav>
    </>
  );
}

export default Navbar;