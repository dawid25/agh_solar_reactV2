import React from 'react';
import '../App.css';
import './GPSpos.css';
import L, { marker } from "leaflet"
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import logo from './../images/lodka.png';

const {InfluxDB} = require('@influxdata/influxdb-client')

// You can generate a Token from the "Tokens Tab" in the UI
const token = '3hcKxNyht5xge7LudFT7B5J7S4vEHi8ByR7NeB6q9jiRN_IQOGTrGitF8yUC28PZjsspRELcoxzzLTzvu3zb5g=='
const org = 'Solar'
const bucket = 'solar'
const url = 'http://194.59.140.149:8086'
//const client = new InfluxDB({url: 'http://194.59.140.149:8086', token: token})

const queryApi = new InfluxDB({url, token}).getQueryApi(org)
/** 
 //Write data to InfluxDB
const {Point} = require('@influxdata/influxdb-client')
const writeApi = client.getWriteApi(org, bucket)
writeApi.useDefaultTags({host: 'host1'})

function writeData() {
  const point = new Point('mem')
    .floatField('used_percent', 44.43234543)
  writeApi.writePoint(point)
  writeApi
      .close()
      .then(() => {
          console.log('FINISHED')
      })
      .catch(e => {
          console.error(e)
          console.log('\\nFinished ERROR')
      })
}
*/

/** Execute a query and receive line table metadata and rows. */
const fluxQuery = 'from(bucket: "solar") |> range(start: -1m) |> filter(fn: (r) => r["_measurement"] == "cpu") |> filter(fn: (r) => r["_field"] == "usage_system") |> filter(fn: (r) => r["cpu"] == "cpu-total") |> yield(name: "mean" )'


var test = 0.1
var ROBURlat = 0.0
var ROBURlon = 0.0
// in progress
const fluxObserver = {
  next(row, tableMeta) {
    const o = tableMeta.toObject(row)
    console.log(
      `${o._time} ${o._measurement} in ${o.region} (${o.sensor_id}): ${o._field}=${o._value}`
    )
      test = parseFloat(o._value);
      console.log(`test= ${test}`)
      ROBURcords(test);
  },
  error(error) {
    console.error(error)
    console.log('\nFinished ERROR')
  },
  complete() {
    console.log('\nFinished SUCCESS')
  }
}


queryApi.queryRows(fluxQuery, fluxObserver)
/*
function GetIcon() {
  return L.icon(  {
    iconUrl: require('./../../src/images/lodka.png'),
    iconSize: [40, 24],
    iconAnchor: [20, 12]
  })
}
*/

function ROBURcords(test){
  let test2 = test
  let json = {'lat': test2, 'lon': 19.909388127380193}
  return [json.lat, json.lon];
}

function Cords() {
  let lat = 50.067763881479046;
  let lon = 19.909388127380193;
  return [lat, lon];
}

var BoatIcon = L.icon({
  iconUrl: 'https://leafletjs.com/examples/custom-icons/leaf-green.png',//some shit logo,
  iconSize: [38, 95],
  iconAnchor: [22, 94],
  shadowAnchor: [4, 64],
  popupAnchor: [12, -90]
})

/** ISS coordinates
 * const apiISS_url = 'https://api.wheretheiss.at/v1/satellites/25544';

let firstTime = true;

async function getISS() {
  const response = await fetch(apiISS_url);
  const data = await response.json();
  const {  latitude, longitude } = data;

  if(firstTime){
  firstTime = false;
            }
            return [latitude, longitude];
          }
          
getISS();
          
setInterval(getISS, 1000);
 */


function GPSPos() {
  const position = Cords();
    return (
        <div className='GPS-container'>
          <h1>data: {test} </h1>
            <MapContainer center={position} zoom={10} scrollWheelZoom={true} doubleClickZoom={true}>
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker position={position} icon={BoatIcon}>
                <Popup>
                  R.O.B.U.R is here
                </Popup>
              </Marker>
            </MapContainer>
        </div> 
    );
}


export default GPSPos;