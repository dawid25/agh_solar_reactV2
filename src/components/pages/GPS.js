import React from 'react';
import '../../App.css';
import HeroSection from '../HeroSection';
import Footer from '../Footer';
import GPSPos from '../GPSpos';

function GPSPosition() {
  return (
    <>
      <GPSPos />
      <Footer />
    </>
  );
}

export default GPSPosition;