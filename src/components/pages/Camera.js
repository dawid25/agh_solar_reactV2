import React from 'react';
import '../../App.css';
import Footer from '../Footer';
import CamStream from '../CamStream'

function Camera() {
  return (
    <>
      <CamStream />
      <Footer />
    </>
  );
}

export default  Camera;
 