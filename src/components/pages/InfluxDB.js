import React from 'react';
import '../../App.css';
import Footer from '../Footer';
import HeroSection from '../HeroSection';

function InfluxDB() {
  return (
    <>
      <HeroSection />
      <Footer />
    </>
  );
}

export default  InfluxDB;