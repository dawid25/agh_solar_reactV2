import '../App.css';
import YouTube from 'react-youtube';
import React, { Component } from 'react';
import './CamStream.css';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      playerPlayVideo: () => {}
    }
    this.videoOnReady = this.videoOnReady.bind(this);
  }

  videoOnReady(event) {
    // access to player in all event handlers via event.target
    this.setState({
      playerPlayVideo: event.target.playVideo,
    });
  }

  render() { 
    const opts = {
      height: '500',//390
      width: '820',//640
      playerVars: {
      autoplay: 1,
      mute: 0,
      }
    };

    return ( 
      
      <div className="Cam-Container">
        <h1>MY VIEW...SOON:)</h1>
        <YouTube videoId="ou6gO2pJ5V4" opts={opts} onReady={this.videoOnReady}/>
      </div>
    );
  }
}
 
export default App;